# Terraform Puppet Facts

Easy way of passing Terrafom variables as Puppet facts to instances.

## Terraform Version Compatibility
Module Version|Terraform Version
---|---
v2.0.0|0.12.x
v1.0.0|0.11.x

## Example Usage

```hcl
module "facts" {
  source = "../modules/tf-userdata-puppet_facts"

  facts_map = {
    fact1 = "foo"
    fact2 = "bar"
    fact3 = module.woohoo.output1
  }
}

data "template_cloudinit_config" "web" {
  part {
    filename     = "facts.py"
    content_type = "text/x-shellscript"
    content      = module.facts.rendered
  }
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  [...]
  user_data     = data.template_cloudinit_config.web.rendered
}
```
