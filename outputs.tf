output "rendered" {
  description = "Add this tag to EC2 instances to allow access"
  value       = join("", data.template_file.facts.*.rendered)
}
