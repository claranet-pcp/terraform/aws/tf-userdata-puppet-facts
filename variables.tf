variable "facts_map" {
  # No type restriction as it can vary based on facts needed
  description = "Map containing facts"
}

variable "facts_file" {
  type        = string
  description = "Name of the file to save facts to. Has to end with '.json'"
  default     = "terraform.json"
}

variable "facts_dir" {
  type        = string
  description = "Custom facter facts directory"
  default     = "/etc/facter/facts.d"
}

variable "enabled" {
   type        = bool
   description = "Enable or disable resources"
   default     = true
 }
