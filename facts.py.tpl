#!/usr/bin/python3

import json
import os

FACTS_DIR = '${facts_dir}'
FACTS_FILE = os.path.join(FACTS_DIR, '${facts_file}')

facts = json.dumps(${facts})

if not os.path.isdir(FACTS_DIR):
    os.makedirs(FACTS_DIR, mode=0o755)

with open(FACTS_FILE, 'w') as f:
    f.write(facts)
