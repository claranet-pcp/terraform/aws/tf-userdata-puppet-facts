data "template_file" "facts" {
  count    = var.enabled
  template = file("${path.module}/facts.py.tpl")

  vars = {
    facts_dir  = var.facts_dir
    facts_file = var.facts_file
    facts      = jsonencode(var.facts_map)
  }
}
